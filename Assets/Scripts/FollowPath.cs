﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPath : MonoBehaviour {

	public PathDisplay PathToFollow;
	
	public int CurrentWayPointID = 0;
	public float mSpeed = 0.0f;
	private float reachDistance = 1.0f;
	public float rotationSpeed = 5.0f;
	public string pathName;
	
	Vector3 last_position;
	Vector3 current_position;

	// Use this for initialization
	void Start () {
		//PathToFollow = GameObject.Find (pathName).GetComponent<PathDisplay> ();
		last_position = transform.position;
	}
	
	public void speedUp() {
		mSpeed += 10;
	}
	
	// Update is called once per frame
	void Update () {
		float distance = Vector3.Distance(PathToFollow.path_objs[CurrentWayPointID].position, transform.position);
		transform.position = Vector3.MoveTowards(transform.position, PathToFollow.path_objs[CurrentWayPointID].position, Time.deltaTime * mSpeed);
		
		var rotation = Quaternion.LookRotation (PathToFollow.path_objs[CurrentWayPointID].position - transform.position);
		transform.rotation = Quaternion.Slerp (transform.rotation, rotation, Time.deltaTime * rotationSpeed);
		
		if (distance <= reachDistance) {
			CurrentWayPointID++;
		}
		
		if (CurrentWayPointID >= PathToFollow.path_objs.Count) {
			CurrentWayPointID = 0;
		}
		
		if (mSpeed > 0) {
			mSpeed*=0.95f;
		}
	}
}
