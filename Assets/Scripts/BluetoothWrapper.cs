﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BluetoothWrapper : MonoBehaviour {

	public FollowPath ballMover;
	
	private Rigidbody rb;
	public float speed;
	private AndroidJavaObject btObj = null;
	private AndroidJavaObject activityContext = null;
	public Text countText;
	public Text winText;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		//var plugin = new AndroidJavaClass("team.bike.com.btuplugin.Bluetooth");
		
		/*using (AndroidJavaClass jc = new AndroidJavaClass("com.bike.team.btuplugin.Bluetooth")) { 
			//jc.CallStatic ("UnitySendMessage", "Jump", "performJump", "doJump"); 
			jc.CallStatic("start");
		}*/
		
		/*cls_UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		activityContext = cls_UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
		btObj = new AndroidJavaClass("com.lab.Android.AndroidClientPlugin");*/
		
		//Vector3 movement = new Vector3(1f,0f,0f);
		//rb.AddForce(movement * 1000);
		//SetCountText ();
		
		if (btObj == null) {
			using(AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer")) {
				activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
			}
			
			using(AndroidJavaClass pluginClass = new AndroidJavaClass("com.bike.team.btuplugin.Bluetooth")) {
				if (pluginClass != null) {
					btObj = pluginClass.CallStatic<AndroidJavaObject>("instance");
					btObj.Call("setContext", activityContext);
					activityContext.Call("runOnUiThread", new AndroidJavaRunnable(() => {
						btObj.Call("showMessage", "This is a toast msg");
					}));
				}
			}
		}

	}

	// Update is called once per frame
	/*void Update () {

	}*/
	
	public void move(string message) {
		ballMover.speedUp();
		//Vector3 movement = new Vector3(0f,0f,1f);
		//rb.AddForce(movement * speed);
	}
	
	public void fail(string message) {
		winText.text = message + ": Failed to connect";
	}
	
	void SetCountText () {
		countText.text = "Count: ";
		winText.text = "Hi";
	}
}
